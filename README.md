# Dotfiles
My personal config for Zsh, Neovim, i3wm, Git, X11, Python, Rust, Less, Zathura, GDB, Screen, Total Commander, Windows Terminal and Windows CMD.

To initialize all submodules, clone this repo with:
```bash
git clone --recursive git@gitlab.com:chrismit3s/dotfiles
```

## Install
### Linux
1. Install git (see [Requirements](#requirements))
2. Clone the repo with
```bash
git clone --recursive git@gitlab.com:chrismit3s/dotfiles ~/.dotfiles
```

3. Run the `bin/symlink` script to create symlinks

### Windows
1. Install git (see [Requirements](#requirements))
2. Clone the repo with
```bash
git clone --recursive git@gitlab.com:chrismit3s/dotfiles ~/.dotfiles
```

3. Create symlinks by hand (script is WIP)
4. Create a `DOTFILES` environment variable pointing to the root of this repo
5. Run `scripts/set-cmdrc.reg` to make CMD source cmdrc
6. Run `scripts/tc-standard.reg` to make Total Commander the default file explorer

## Requirements

| Package          | `pacman -Syu` | `yay -Syu`            | `apt install`                     | `snap install` | `choco install`                 | `winget install -e --id`        |
| ---------------- | ------------- | --------------------- | --------------------------------- | -------------- | ------------------------------- | ------------------------------- |
| zsh              | `zsh`         |                       | `zsh`                             |                |                                 |                                 |
| Neovim           | `neovim`      |                       | `neovim`                          |                | `neovim`                        | `Neovim.Neovim`                 |
| i3               | `i3-wm`       |                       | `i3`                              |                |                                 |                                 |
| git              | `git`         |                       | `git`                             |                | `git`                           | `Git.Git`                       |
| ssh              | `ssh`         |                       | `ssh`                             |                | `openssh`                       | [not yet][openssh-winget-issue] |
| Python 3         | `python3`     |                       | `python3-full`                    |                | `python`                        | `Python.Python.3`               |
| rustup           | `rustup`      |                       | [see below](#rustup)              |                | `rustup` ([see below](#rustup)) | [see below](#rustup)            |
| less             | `less`        |                       | `less`                            |                |                                 |                                 |
| Zathura          | `zathura`     |                       | `zathura`                         |                |                                 |                                 |
| gdb/gcc          | `gdb gcc`     |                       | `gdb gcc`                         |                | `mingw`                         |                                 |
| screen           | `screen`      |                       | `screen`                          |                |                                 |                                 |
| rclone           | `rclone`      |                       | [see below](#rclone)              |                |                                 |                                 |
| Powerline fonts  |               | `powerline-fonts-git` | [see below](#powerline-fonts)     |                |                                 | [see below](#powerline-fonts)   |
| Discord          | `discord`     |                       |                                   | `discord`      | `discord`                       | `Discord.Discord`               |
| Google Chrome    |               | `google-chrome`       | [see below](#google-chrome)       |                | `googlechrome`                  | `Google.Chrome`                 |
| Total Commander  |               |                       |                                   |                | `totalcommander`                | `Ghisler.TotalCommander`        |
| Windows Terminal |               |                       |                                   |                | `microsoft-windows-terminal`    | `Microsoft.WindowsTerminal`     |

### rustup
You can't install `rustup` with `apt` and `winget`. Instead use
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
on linux or download `rustup-init.exe` from [here][rustup-exe-dl] on windows.
The chocolatey install also has [some issues][rustup-choco-issue].

### rclone
You can't install `rclone` with `apt`. Instead use
```bash
curl https://rclone.org/install.sh | sudo bash
```

### Powerline fonts
You can't install [Powerline fonts][powerline-fonts] with `apt`, `choco` and `winget`. Instead use
```bash
git clone https://github.com/powerline/fonts --depth=1
cd fonts
./install.{sh,ps1}
```

### Google Chrome
You can't install Google Chrome with `apt`. Instead, download [`google-chrome.deb`][chrome-deb-dl] and follow the instructions.

## Resources
### Zsh
#### What shell does what? [[source][shell-types]]
Every shell has two properties:
* (Non-)Interactive: takes user input
* (Non-)Login: when a user signed in to the shell specifically

Examples:

|           | Interactive                   | Non-interactive            |
| --------- | ----------------------------- | -------------------------- |
| Login     | `ssh`ing into another machine | very rare                  |
| Non-login | runs when you open a terminal | runs when you run a script |

#### Which shell sources what config files? [[source][shell-startup-article]]
![image is from the article linked above][shell-startup-flowchart]

#### What belongs in which config file? [[source][zsh-config-files]]
In order of sourcing:

* `.zshenv` is always sourced. It often contains exported variables that should be available to other programs. For example, `$PATH`, `$EDITOR`, and `$PAGER` are often set in `.zshenv`. Also, you can set `$ZDOTDIR` in `.zshenv` to specify an alternative location for the rest of your zsh configuration.
* `.zprofile` is for login shells. It is basically the same as `.zlogin` except that it's sourced before `.zshrc` whereas `.zlogin` is sourced after `.zshrc`. According to the zsh documentation, "`.zprofile` is meant as an alternative to `.zlogin` for ksh fans; the two are not intended to be used together, although this could certainly be done if desired."
* `.zshrc` is for interactive shells. You set options for the interactive shell there with the setopt and unsetopt commands. You can also load shell modules, set your history options, change your prompt, set up zle and completion, et cetera. You also set any variables that are only used in the interactive shell (e.g. `$LS_COLORS`).
* `.zlogin` is for login shells. It is sourced on the start of a login shell but after `.zshrc`, if the shell is also interactive. This file is often used to start X using startx. Some systems start X on boot, so this file is not always very useful.
* `.zlogout` is sometimes used to clear and reset the terminal. It is called when exiting, not when opening.

### GUI theming (Qt and GTK)
* a nice theme available for Qt and GTK is Breeze, install with `sudo apt install breeze breeze-gtk-theme`
* to set the Qt theme, use `qt5ct`, install with `sudo apt install qt5ct`
* to set the GTK theme, use `lxappearance`, install with `sudo apt install lxappearance`
* start both applications and select Breeze

### XDG base directories [[spec][xdg-base-dir-spec]]

| Variable           | Default value                 | Purpose | Examples |
| ------------------ | ----------------------------- | ------- | -------- |
| `$XDG_DATA_HOME`   | `~/.local/share`              | defines the base directory relative to which user-specific data files should be stored | Fonts, application assets, logs |
| `$XDG_DATA_DIRS`   | `/usr/local/share:/usr/share` | defines the colon-separated, preference-ordered set of base directories to search for data files in addition to the `$XDG_DATA_HOME` base directory | Fonts, application assets, logs |
| `$XDG_CONFIG_HOME` | `~/.config`                   | defines the base directory relative to which user-specific configuration files should be stored | Configuration files |
| `$XDG_CONFIG_DIRS` | `/etc/xdg`                    | defines the colon-separated, preference-ordered set of base directories to search for configuration files in addition to the `$XDG_CONFIG_HOME` base directory | Configuration files |
| `$XDG_STATE_HOME`  | `~/.local/state`              | defines the base directory relative to which user-specific state files should be stored | Logs, history files, files that store the state of an application, non-essential but still important to the application |
| `$XDG_CACHE_HOME`  | `~/.cache`                    | defines the base directory relative to which user-specific non-essential data files should be stored | Lookups, cached data, files that should only take time to rebuild and only serve the purpose of speeding up an application, could be deleted without data loss |

[shell-startup-article]: https://blog.flowblok.id.au/2013-02/shell-startup-scripts.html
[shell-startup-flowchart]: https://blog.flowblok.id.au/static/images/shell-startup.png
[shell-types]: https://unix.stackexchange.com/a/46856
[zsh-config-files]: https://unix.stackexchange.com/a/71258
[openssh-winget-issue]: https://github.com/PowerShell/Win32-OpenSSH/issues/1896#issuecomment-1090503924
[rustup-choco-issue]: https://github.com/rust-lang/rustup/issues/912
[rustup-exe-dl]: https://win.rustup.rs/x86_64
[chrome-deb-dl]: https://www.google.com/chrome/
[powerline-fonts]: https://github.com/powerline/fonts
[xdg-base-dir-spec]: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
