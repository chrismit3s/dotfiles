#!/usr/bin/env zsh

set -uo pipefail

usage() {
    echo "USAGE: $0 <file> [-n <n>]"
    echo "<file> file of the imported scan"
    echo "import the <n>th last scan, defaults to 0 (the newest scan)"
    exit 1
}

if [[ $# -eq 0 ]]; then
    usage
fi

file=$1
shift

n=0
while [[ $# -gt 0 ]]; do
    case $1 in
        "-n")
            shift
            # number check from https://stackoverflow.com/a/2704760/7841202
            if [[ -n "$1" && -z "${1##*[!0-9]*}" ]]; then
                echo "'$1' is not a valid number"
                usage
            fi

            n=$1
            shift
            ;;
        *)
            echo "Invalid option $1"
            usage
    esac
done

pdfs=$(ssh cip ls '/proj/scan/$USER*.pdf')
npdfs=$(echo $pdfs | wc -l)
pdf=$(echo $pdfs | sed "$(($npdfs - $n))q;d")

if [[ -e $file ]]; then
    echo "$file already exists"
    usage
fi

scp cip:$pdf $file
