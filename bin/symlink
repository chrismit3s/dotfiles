#!/usr/bin/env zsh

# easy debugging
set -uo pipefail

# ensure we are at the root of the dotfiles repo (${0:A:h} is the script directory, $DOTFILES/bin in this case)
cd "${0:A:h}/.."

# load env vars (mainly XDG_CONFIG_HOME and ZDOTDIR)
source ./zsh/zshenv

# colors and escape codes
clear_line=$'\33[2K\r'
declare -A colors
colors[clear]=$'\033[0m'
colors[black]=$'\033[0;30m'
colors[red]=$'\033[0;31m'
colors[green]=$'\033[0;32m'
colors[orange]=$'\033[0;33m'
colors[blue]=$'\033[0;34m'
colors[purple]=$'\033[0;35m'
colors[cyan]=$'\033[0;36m'
colors[white]=$'\033[0;37m'
colors[black-bold]=$'\033[1;30m'
colors[red-bold]=$'\033[1;31m'
colors[green-bold]=$'\033[1;32m'
colors[yellow-bold]=$'\033[1;33m'
colors[blue-bold]=$'\033[1;34m'
colors[purple-bold]=$'\033[1;35m'
colors[cyan-bold]=$'\033[1;36m'
colors[white-bold]=$'\033[1;37m'

skip="$colors[white-bold]skip$colors[clear]"
link="$colors[blue-bold]link$colors[clear]"
move="$colors[red-bold]move$colors[clear]"

symlink() {
    source=$1
    target=$2

    source=$(realpath $source)
    dir=$(dirname $target)
    link_target=$(readlink --canonicalize $target)

    mkdir --parents $dir

    # target is a link and points to source
    if [[ -L $target ]] && [[ $link_target == $source ]]; then
        echo "[$skip] $target already points to $source"
        return
    fi

    # target exists
    if [[ -e $target ]]; then
        echo "[$move] $target moved to $target.old"
        mv $target $target.old
    fi

    echo "[$link] $target points to $source"
    ln -s $source $target
}

# symlink zsh config
compile-zconfig
symlink $DOTFILES/zsh/zshenv ~/.zshenv
symlink $DOTFILES/zsh/zshenv.zwc ~/.zshenv.zwc
for f in zpreztorc zpreztorc.zwc zprofile zprofile.zwc zshrc zshrc.zwc zshenv zshenv.zwc; do
    symlink $DOTFILES/zsh/$f $ZDOTDIR/.$f
done

# symlink i3 config
symlink $DOTFILES/i3/config $XDG_CONFIG_HOME/i3/config
symlink $DOTFILES/i3/status.toml $XDG_CONFIG_HOME/i3status-rust/config.toml

# symlink systemd user units
for unit in $DOTFILES/systemd/*; do
    symlink $unit $XDG_CONFIG_HOME/systemd/user/${unit#$DOTFILES/systemd/}
done

# symlink nvim config
symlink $DOTFILES/nvim/init.lua $XDG_CONFIG_HOME/nvim/init.lua
symlink $DOTFILES/nvim/lua $XDG_CONFIG_HOME/nvim/lua

# symlink rustfmt config
symlink $DOTFILES/rust/rustfmt.toml $XDG_CONFIG_HOME/rustfmt/rustfmt.toml

# symlink ipython config
symlink $DOTFILES/python/ipython.py ${IPYTHONDIR:-$XDG_CONFIG_HOME/ipython}/profile_default/ipython_config.py

# symlink git config
for unit in $DOTFILES/git/*; do
    symlink $unit ~/.${unit#$DOTFILES/git/}
done

# symlink lesskey
symlink $DOTFILES/lesskey ~/.lesskey

# symlink zathura config
symlink $DOTFILES/zathurarc $XDG_CONFIG_HOME/zathura/zathurarc

# symlink gdb config
symlink $DOTFILES/gdbinit $XDG_CONFIG_HOME/gdb/gdbinit

# symlink x11 config
symlink $DOTFILES/x11/xresources ~/.Xresources
symlink $DOTFILES/x11/xsessionrc ~/.xsessionrc
