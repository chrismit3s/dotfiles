from pathlib import Path
from argparse import ArgumentParser
from os import environ


WEBAPPS = {
    "whatsapp": "https://web.whatsapp.com",
    "telegram": "https://web.telegram.org",
    "mattermost": "https://chat.robotics-erlangen.de",
    "gmail": "https://mail.google.com",
    "gcalendar": "https://calendar.google.com",
    "gphotos": "https://photos.google.com",
    "gdocs": "https://docs.google.com/document/u/0/",
    "gsheets": "https://docs.google.com/spreadsheets/u/0/",
}
TEMPLATE = "#!/usr/bin/env zsh\nexec gchrome --app={}"
DOTFILES = Path(environ["DOTFILES"])


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--force", "-f", action="store_true", help="Overwrite existing scripts")
    args = parser.parse_args()

    for name, url in WEBAPPS.items():
        path = DOTFILES / "bin" / "webapps" / name
        if not args.force and path.exists():
            print(f"SKIP: {path}")
            continue
        with path.open("w+") as f:
            print(TEMPLATE.format(url), file=f)
        path.chmod(0o755)
        print(f"CREATED: {path}")
