setlocal shiftwidth=4
setlocal softtabstop=4
setlocal expandtab
setlocal foldmethod=syntax

nnoremap <buffer> <localleader>lm w"myb:vert Man 3p <C-r>m<CR>
