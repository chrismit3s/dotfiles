CoqStart

setlocal shiftwidth=4
setlocal softtabstop=4
setlocal expandtab
setlocal foldmethod=indent

" dont wrap lines
setlocal textwidth=0
setlocal wrapmargin=0

" map vimtex commands
inoremap <buffer> <Down> <Cmd>CoqNext<CR>
inoremap <buffer> <Up> <Cmd>CoqUndo<CR>
inoremap <buffer> <M-O>b <Cmd>5CoqNext<CR>
inoremap <buffer> <M-O>a <Cmd>5CoqUndo<CR>
nnoremap <buffer> <Down> <Cmd>CoqNext<CR>
nnoremap <buffer> <Up> <Cmd>CoqUndo<CR>
nnoremap <buffer> <M-O>b <Cmd>5CoqNext<CR>
nnoremap <buffer> <M-O>a <Cmd>5CoqUndo<CR>

nnoremap <buffer> <localleader>cc <Cmd>CoqToLine<CR>
nnoremap <buffer> <localleader>cC <Cmd>$CoqToLine<CR>
