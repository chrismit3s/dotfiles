setlocal tabstop=2
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal noexpandtab
setlocal wrap

command! -buffer View !$BROWSER %

" umlaute type themselves
inoremap <buffer> <char-246> ö
inoremap <buffer> <char-214> Ö
inoremap <buffer> <char-228> ä
inoremap <buffer> <char-196> Ä
inoremap <buffer> <char-252> ü
inoremap <buffer> <char-220> Ü
