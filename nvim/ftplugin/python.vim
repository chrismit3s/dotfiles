setlocal shiftwidth=4
setlocal softtabstop=4
setlocal expandtab
setlocal foldmethod=indent

command! -buffer Format call Black()

augroup FormatOnSave
    autocmd!
    autocmd BufWritePre,FileWritePre * :call BlackSync()
augroup END
