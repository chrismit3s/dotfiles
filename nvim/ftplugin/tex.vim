setlocal shiftwidth=4
setlocal softtabstop=4
setlocal expandtab
setlocal foldmethod=indent

" dont wrap lines
setlocal textwidth=0
setlocal wrapmargin=0

command! -buffer Build !pdflatex --shell-escape -max-print-line=120 -interaction=nonstopmode -halt-on-error %
command! -buffer B write|Build

" map vimtex commands
nnoremap <buffer> <localleader>tv <Plug>(vimtex-view)
nnoremap <buffer> <localleader>te <Plug>(vimtex-errors)
nnoremap <buffer> <localleader>to <Plug>(vimtex-compile-output)
nnoremap <buffer> <localleader>tx <Plug>(vimtex-clean)
nnoremap <buffer> <localleader>tc <Plug>(vimtex-compile)
xnoremap <buffer> <localleader>tc <Plug>(vimtex-compile-selected)
nnoremap <buffer> <localleader>tp <Plug>(vimtex-stop)
nnoremap <buffer> <localleader>tt <Plug>(vimtex-toc-toggle)

" umlaute type themselves
inoremap <buffer> <char-246> ö
inoremap <buffer> <char-214> Ö
inoremap <buffer> <char-228> ä
inoremap <buffer> <char-196> Ä
inoremap <buffer> <char-252> ü
inoremap <buffer> <char-220> Ü

" command mappings
iabbrev <buffer> (( \left(
iabbrev <buffer> )) \right)
iabbrev <buffer> [[ \left[
iabbrev <buffer> ]] \right]
iabbrev <buffer> ** \cdot
