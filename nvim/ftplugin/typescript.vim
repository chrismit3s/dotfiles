setlocal tabstop=4
setlocal shiftwidth=4
setlocal softtabstop=4
setlocal noexpandtab
setlocal foldmethod=indent

" to read the modifications for the background npm fix run
setlocal autoread
setlocal updatetime=100

command! -buffer Format !npm run lint-only -- --fix %:p

augroup FormatOnSave
    autocmd!
    autocmd BufWritePost,FileWritePost * :silent !npm run lint-only -- --fix %:p &
    autocmd CursorHold * :checktime
augroup END
