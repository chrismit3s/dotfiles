-- See :h map-table
local MODES = {
    normal = "n",
    insert = "i",
    visual = "v", -- visual and select mode
    xvisual = "x", -- only visual
    operator = "o",
    command = "c",
    terminal = "t"
}

return { MODES = MODES }
