vim.g.mapleader = ","
vim.g.maplocalleader = " "

require("mappings/folding")
require("mappings/movement")
require("mappings/parens")
require("mappings/scrolling")
require("mappings/window")

local utils = require("utils")
local MODES = require("consts").MODES

-- disable default mappings
vim.g.gitgutter_map_keys = 0  -- gitgutter
vim.g.coqtail_nomap = 1  -- coq plugin
vim.g.no_man_maps = 1  -- man page plugin

-- unundo
vim.keymap.set(MODES.normal, "U", "<C-r>")

-- undotree
vim.keymap.set(MODES.normal, "<localleader>u", function() vim.call("undotree#UndotreeToggle") end)
vim.g.Undotree_CustomMap = function()
    local opts = { buffer = true, remap = true, silent = true }
    vim.keymap.set(MODES.normal, "U", "<C-r>", opts)
    -- This would be the correct way to do it, but thats suuuper busted, so the workaround above has to suffice :(
    --vim.keymap.set(MODES.normal, "u", "<Plug>UndotreeUndo()", opts)
    --vim.keymap.set(MODES.normal, "U", "<Plug>UndotreeRedo()", opts)
    --vim.keymap.set(MODES.normal, "K", "<Plug>UndotreePreviousState()", opts)
    --vim.keymap.set(MODES.normal, "L", "<Plug>UndotreeNextState()", opts)
end

-- delete word with ctrl-backspace
local ic = { MODES.insert, MODES.command }
vim.keymap.set(MODES.normal, "<C-h>", "<C-w>")

-- copy to end of line, make Y like D
vim.keymap.set(MODES.normal, "Y", "y$")

-- git (fugitive and gitgutter)
local m = { MODES.normal, MODES.visual }
vim.keymap.set(m, "<localleader>gs", function()
    if vim.fn.mode() == "n" then
        vim.call("gitgutter#hunk#stage")
    else
        local s = vim.fn.getpos("'<")[2]
        local e = vim.fn.getpos("'>")[2]
        vim.call("gitgutter#hunk#stage", s, e)
    end
end)
vim.keymap.set(MODES.normal, "<localleader>gu", function() vim.call("gitgutter#hunk#undo") end)
vim.keymap.set(MODES.normal, "<localleader>gv", function() vim.call("gitgutter#hunk#preview") end)
vim.keymap.set(MODES.normal, "<localleader>gN", function() vim.call("gitgutter#hunk#prev_hunk", 1) end)
vim.keymap.set(MODES.normal, "<localleader>gn", function() vim.call("gitgutter#hunk#next_hunk", 1) end)
vim.keymap.set(MODES.normal, "<localleader>gm", "/^[<>=<bar>]\\{7\\}<CR>", { silent = true })
vim.keymap.set(MODES.normal, "<localleader>gl", function() vim.cmd("vertical Git log") end)
vim.keymap.set(MODES.normal, "<localleader>gh", function() vim.cmd("Git hist 5 --color=never") end)
vim.keymap.set(MODES.normal, "<localleader>gt", function() vim.cmd("Git status") end)
vim.keymap.set(MODES.normal, "<localleader>gr", function() vim.cmd("Git restore --staged %") end)
vim.keymap.set(MODES.normal, "<localleader>gS", function() vim.cmd("Git add %") end)
vim.keymap.set(MODES.normal, "<localleader>gb", function() vim.cmd("Git blame") end)
vim.keymap.set(MODES.normal, "<localleader>ge", "<cmd>let g:gitgutter_preview_win_floating=v:false<CR><Plug>(GitGutterPreviewHunk)<cmd>let g:gitgutter_preview_win_floating=v:false<CR><C-w>P", { silent = true })  -- TODO

-- close buffer
vim.keymap.set(MODES.normal, "<leader>d", function()
    local bcount = #utils.bufs_of_type("normal")
    if bcount <= 1 then
        vim.cmd.quit()
    else
        vim.cmd.bnext()
        vim.cmd.bdelete("#")
    end
end)

-- turn off search highlighting
vim.keymap.set(MODES.normal, "<leader>n", vim.cmd.nohlsearch, { silent = true })

-- search for selection
vim.keymap.set(MODES.visual, "*", '"vy/<C-R>=escape(@v, "/\\[].*")<CR><CR>')
vim.keymap.set(MODES.visual, "#", '"vy?<C-R>=escape(@v, "/\\[].*")<CR><CR>')

-- compute selection
vim.keymap.set(MODES.visual, "=", 'c<C-r>=<C-r>"<CR><Esc>')

-- fzf
vim.keymap.set(MODES.normal, "<C-f>", "<cmd>Files<CR>", { silent = true })
vim.keymap.set(MODES.normal, "<C-g>", "<cmd>GitFiles<CR>", { silent = true })
vim.keymap.set(MODES.normal, "<C-c>", "<cmd>Rg<CR>", { silent = true })

-- launch macros with Q instead of @
vim.keymap.set(MODES.normal, "Q", "@")

-- enable escape in terminal mode
vim.keymap.set(MODES.terminal, "<Esc>", "<C-\\><C-n>")
