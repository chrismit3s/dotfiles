local MODES = require("../consts").MODES

-- folding (from https://stackoverflow.com/a/62635630 and https://stackoverflow.com/a/21634546)
vim.keymap.set(MODES.normal, "hh", "za")
vim.keymap.set(MODES.normal, "ho", "zo")
vim.keymap.set(MODES.normal, "hc", "zc")
vim.keymap.set(MODES.normal, "hO", "zO")
vim.keymap.set(MODES.normal, "hC", "zcVj:foldclose!<CR>", { silent = true })
vim.keymap.set(MODES.normal, "HO", "zR")
vim.keymap.set(MODES.normal, "HC", "zM")
