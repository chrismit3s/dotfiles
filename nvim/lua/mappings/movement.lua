local MODES = require("../consts").MODES


-- in general, map hjkl to jklö
local m = { MODES.normal, MODES.operator, MODES.xvisual }
vim.keymap.set(m, "j", "h")
vim.keymap.set(m, "ö", "l")

-- gj/gk move in wrapped lines like you'd expect
vim.keymap.set(MODES.normal, "k", "gj")
vim.keymap.set(MODES.normal, "l", "gk")

-- move full lines in operator pending mode (e.g. dk deletes this line and the one below)
vim.keymap.set(MODES.operator, "k", "j")
vim.keymap.set(MODES.operator, "l", "k")

-- in plain visual mode, move in lines with gj/gk, in the other visual
-- modes, move with j/k (see https://vi.stackexchange.com/a/9279)
vim.keymap.set(MODES.xvisual, "k", "mode() ==# 'v' ? 'gj' : 'j'", { expr = true })
vim.keymap.set(MODES.xvisual, "l", "mode() ==# 'v' ? 'gk' : 'k'", { expr = true })
vim.keymap.set(MODES.xvisual, "gk", "mode() ==# 'v' ? 'j'  : 'gj'", { expr = true })
vim.keymap.set(MODES.xvisual, "gl", "mode() ==# 'v' ? 'k'  : 'gk'", { expr = true })

-- the same again for capital letters and larger movements
local m = { MODES.normal, MODES.operator, MODES.xvisual }
vim.keymap.set(m, "J", "b")
vim.keymap.set(m, "Ö", "w")

-- gj/gk move in wrapped lines like you'd expect
vim.keymap.set(MODES.normal, "K", "3gj")
vim.keymap.set(MODES.normal, "L", "3gk")

-- move full lines in operator pending mode (e.g. dk deletes this line and the one below)
vim.keymap.set(MODES.operator, "K", "3j")
vim.keymap.set(MODES.operator, "L", "3k")

-- in plain visual mode, move in lines with gj/gk, in the other visual
-- modes, move with j/k (see https://vi.stackexchange.com/a/9279)
vim.keymap.set(MODES.xvisual, "K", "mode() ==# 'v' ? '3gj' : '3j'", { expr = true })
vim.keymap.set(MODES.xvisual, "L", "mode() ==# 'v' ? '3gk' : '3k'", { expr = true })
vim.keymap.set(MODES.xvisual, "gK", "mode() ==# 'v' ? '3j'  : '3gj'", { expr = true })
vim.keymap.set(MODES.xvisual, "gL", "mode() ==# 'v' ? '3k'  : '3gk'", { expr = true })

-- move in line/file
local m = { MODES.normal, MODES.operator, MODES.visual }
vim.keymap.set(m, "B", "^")
vim.keymap.set(m, "E", "$")
vim.keymap.set(m, "<C-b>", "gg")
vim.keymap.set(m, "<C-e>", "G")
