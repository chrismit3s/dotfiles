local MODES = require("../consts").MODES

-- umlaute oOaAuU to (){}[]
local m = { MODES.insert, MODES.operator, MODES.command }
vim.keymap.set(m, "ö", "(")
vim.keymap.set(m, "Ö", ")")
vim.keymap.set(m, "ä", "{")
vim.keymap.set(m, "Ä", "}")
vim.keymap.set(m, "ü", "[")
vim.keymap.set(m, "Ü", "]")
