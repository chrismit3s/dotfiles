local MODES = require("../consts").MODES

local m = { MODES.normal, MODES.visual }

vim.keymap.set(m, "<C-j>", "5zh")
vim.keymap.set(MODES.insert, "<C-j>", "<C-o>5zh")

vim.keymap.set(m, "<C-k>", "<C-y>")
vim.keymap.set(MODES.insert, "<C-k>", "<C-o><C-y>")

vim.keymap.set(m, "<C-l>", "<C-e>")
vim.keymap.set(MODES.insert, "<C-l>", "<C-o><C-e>")

vim.keymap.set(m, "<C-ö>", "5zl")
vim.keymap.set(MODES.insert, "<C-ö>", "<C-o>5zl")
