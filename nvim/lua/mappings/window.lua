local MODES = require("../consts").MODES

-- move focus
local m = { MODES.normal, MODES.operator }
vim.keymap.set(m, "<M-j>", "<C-w>h")
vim.keymap.set(m, "<M-k>", "<C-w>j")
vim.keymap.set(m, "<M-l>", "<C-w>k")
vim.keymap.set(m, "<M-ö>", "<C-w>l")
vim.keymap.set(MODES.insert, "<M-j>", "<C-o><C-w>h")
vim.keymap.set(MODES.insert, "<M-k>", "<C-o><C-w>j")
vim.keymap.set(MODES.insert, "<M-l>", "<C-o><C-w>k")
vim.keymap.set(MODES.insert, "<M-ö>", "<C-o><C-w>l")

-- cycle through tabs/splits/buffers
vim.keymap.set(m, "<leader>s", "<C-w>w")
vim.keymap.set(m, "<leader>S", "<C-w>W")
vim.keymap.set(m, "<leader>b", vim.cmd.bnext)
vim.keymap.set(m, "<leader>B", vim.cmd.bprev)
