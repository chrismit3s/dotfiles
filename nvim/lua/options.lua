-- where to search for ftplugin files
vim.opt.runtimepath:append { "$DOTFILES/nvim" }

-- big history (see :help shada-: and :help 'history')
vim.opt.shada:append { ":1000000" }

-- tabstops
vim.opt.tabstop = 8  -- how wide a '\t' character is displayed
vim.opt.softtabstop = 4  -- how many spaces to insert when pressing tab
vim.opt.shiftwidth = 4  -- how many spaces correspond to one level of indentation
vim.opt.expandtab = true  -- expand tabs to spaces
vim.opt.autoindent = true  -- copy indentation of previous line

-- display options
vim.opt.lazyredraw = true  -- only redraw when needed
if vim.fn.exists("&smoothscroll") then
    vim.opt.smoothscroll = true  -- scroll virtual lines (when wrap is set)
end
vim.opt.wrap = false  -- dont wrap lines
vim.opt.scrolloff = 5  -- keep 5 lines above and below cursor
vim.opt.sidescrolloff = 10  -- keep 10 characters left and right of cursor
vim.opt.title = true  -- rename terminal
vim.opt.showmatch = true  -- highlight matching brace
vim.opt.cursorline = true  -- highlight current line
vim.opt.signcolumn = "yes:1"  -- keep a signcolumn of exactly one
--vim.opt.completeopt:remove { "preview" }  -- dont open scratch pad on omnifunc
vim.opt.background = "dark"  -- nightmode

-- show non printing characters
vim.opt.list = true
vim.opt.listchars = { trail = "•", tab = ">-" }

-- show line numbers
vim.opt.number = true
vim.opt.numberwidth = 3

-- behaviour
vim.opt.autochdir = false  -- use file path as working directory
vim.opt.autoread = true  -- automatically load file changes (u to undo)
vim.opt.backspace = { "indent", "eol", "nostop" }  -- backspace over everything
vim.opt.confirm = true  -- ask to save instead of complaining
vim.opt.splitright = true  -- open new splits on the right
vim.opt.updatetime = 100  -- write the swapfile every 100ms (also makes gitgutter signs appear faster)
vim.opt.virtualedit = "onemore"  -- allow the cursor be one past the last charater in a line

-- persistent undohistory
vim.opt.undofile = true

-- mouse
vim.opt.mouse = "a"  -- enable mouse
vim.opt.mousehide = true  -- hide the cursor

-- search and replace
vim.opt.incsearch = true  -- search as charcters are entered
vim.opt.hlsearch = true  -- highlight matches
vim.opt.ignorecase = true  -- ignore case when searching
vim.opt.smartcase = true  -- only if there are no capital letters in querry
vim.opt.gdefault = true  -- replace all hits in a line

-- folding  -- TODO overthink these settings
vim.opt.foldnestmax = 6
vim.opt.foldminlines = 2

-- shell
local zsh = vim.fn.exepath("zsh")
if zsh ~= "" then
    vim.opt.shell = zsh
end

-- transparent/opaque background
function transparent_background(make_transparent)
    vim.g.is_transparent = make_transparent
    if make_transparent then
        -- line number column
        vim.cmd.highlight("Normal", "ctermbg=none")
        vim.cmd.highlight("LineNr", "ctermbg=none")

        -- cursorline
        vim.cmd.highlight("CursorLine", "ctermbg=none")
        vim.cmd.highlight("CursorLineNr", "ctermbg=none")

        -- folds
        vim.cmd.highlight("Folded", "ctermbg=none")
    else
        vim.cmd.colorscheme("gruvbox")
    end
end
function toggle_background()
    transparent_background(not vim.g.is_transparent)
end

vim.cmd.syntax("on")
transparent_background(false)

-- Fugitive
vim.api.nvim_create_user_command("Gs", "Git status <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Ga", "Git add <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gc", "Git commit <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gl", "Git log <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gh", "Git hist <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gf", "Git fixup <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gd", "Git diff <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gds", "Git diff --staged <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gps", "Git push <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gpf", "Git push --force <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gpl", "Git pull <args>", { nargs = "*" })
vim.api.nvim_create_user_command("Gra", "Git rebase --abort", { nargs = 0 })
vim.api.nvim_create_user_command("Grc", "Git rebase --continue", { nargs = 0 })

-- Python3
vim.g.python3_host_prog = vim.fn.exepath("python3") or vim.fn.exepath("python")

-- LaTeX
vim.g.tex_flavor = "latex"

-- Vimtex
vim.g.vimtex_view_method = "zathura"
vim.g.vimtex_compiler_latexmk = {
    options = {
        "-pdf",
        "-shell-escape",
        "-verbose",
        "-file-line-error",
        "-synctex=1",
        "-interaction=nonstopmode",
    }
}

local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd

if vim.b.vimtex then
    -- for events, look at :help vimtex-events
    local vimtex = "Vimtex"
    augroup(vimtex, { clear = true })
    autocmd("User", {
        pattern = { "VimtexEventInitPost" },
        group = vimtex,
        command = "VimtexCompile",
    })
    autocmd("User", {
        pattern = { "VimtexEventQuit" },
        group = vimtex,
        command = "VimtexClean",
    })
    autocmd("User", {
        pattern = { "VimtexEventQuit" },
        group = vimtex,
        callback = function()
            local xwin_id = vim.b.vimtex.viewer.xwin_id
            if vim.fn.executable("xdotool") and xwin_id ~= nil and xwin_id > 0 then
                vim.fn.system("xdotool windowclose " .. xwin_id)
            end
        end,
    })
    autocmd("User", {
        pattern = { "VimtexEventCompileSuccess" },
        group = vimtex,
        callback = vim.b.vimtex.viewer.xdo_focus_vim,
    })
    autocmd("User", {
        pattern = { "VimtexEventView" },
        group = vimtex,
        callback = vim.b.vimtex.viewer.xdo_focus_vim,
    })
end

--" NetRW
vim.g.netrw_dirhistmax = 0  -- dont generate .netrwhist files

-- Fzf
vim.g.fzf_preview_window = ""  -- disables the preview window (causes lag and missed keystrokes)

-- Airline
vim.g.airline_powerline_fonts = 1
vim.g.airline_theme = vim.g.colors_name or "default"
vim.g.airline_section_b = "%F %m%r%h%y"
vim.g.airline_section_c = ""
vim.g.airline_section_x = "File:%f"
vim.g.airline_section_y = "Line:%l/%L Col:%c"
vim.g.airline_section_z = "%3l|%2c"
vim.g.airline_whitespace_disabled = true

-- Black
vim.g["black#settings"] = {
    fast = 1,
    line_length = 120,
    target_version = "py312",
}

-- Rust
vim.g.rustfmt_autosave = 1
