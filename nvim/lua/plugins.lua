-- start plugin list
vim.cmd.source("$DOTFILES/nvim/vim-plug/plug.vim")
vim.call("plug#begin")
function plug(name) vim.cmd(string.format("Plug '%s'", name)) end

-- colorschemes
plug("ajmwagar/vim-deus")
plug("altercation/vim-colors-solarized")
plug("challenger-deep-theme/vim")
plug("jonathanfilip/vim-lucius")
plug("morhetz/gruvbox")
plug("romainl/apprentice")
plug("sickill/vim-monokai")
plug("vim-scripts/wombat256.vim")

-- plugins
plug("anuvyklack/nvim-keymap-amend")
plug("anuvyklack/pretty-fold.nvim")
plug("junegunn/fzf")
plug("junegunn/fzf.vim")
plug("mattn/webapi-vim")
plug("scrooloose/nerdtree")
plug("tpope/vim-fugitive")
plug("airblade/vim-gitgutter")
plug("tpope/vim-surround")
plug("vim-airline/vim-airline")
plug("vim-airline/vim-airline-themes")
plug("gioele/vim-autoswap")
plug("nvim-treesitter/nvim-treesitter")
plug("folke/which-key.nvim")
plug("tpope/vim-eunuch")
plug("tpope/vim-speeddating")
plug("tpope/vim-repeat")
plug("mbbill/undotree")

-- completions
plug("hrsh7th/nvim-cmp")
plug("hrsh7th/cmp-buffer")
plug("hrsh7th/cmp-path")
plug("hrsh7th/cmp-nvim-lsp")

-- snippets
plug("hrsh7th/vim-vsnip")
plug("rafamadriz/friendly-snippets")
 
-- language server
plug("neovim/nvim-lspconfig")
plug("williamboman/nvim-lsp-installer")
plug("ray-x/lsp_signature.nvim")

-- language support
plug("dart-lang/dart-vim-plugin")
plug("octol/vim-cpp-enhanced-highlight")
plug("rust-lang/rust.vim")
plug("mfussenegger/nvim-jdtls")
plug("lervag/vimtex")
plug("vim-python/python-syntax")
plug("averms/black-nvim")
plug("leafgarland/typescript-vim")
plug("vim-scripts/coq-syntax")
plug("whonore/Coqtail")
plug("cameron-wags/rainbow_csv.nvim")

-- end plugin list
vim.call("plug#end")
vim.cmd.filetype("plugin", "indent", "on")

require("plugins/lsp")
require("plugins/cmp")

require("nvim-treesitter.configs").setup {
  ensure_installed = { "c", "cpp", "cmake", "rust", "typescript", "dart", "python", "latex", "markdown", "toml", "yaml", "vim", "lua", "clojure" },
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = true,
  },
}

require("pretty-fold").setup { -- TODO replace this plugin?
    fill_char = "-",
    matchup_patterns = {
      { "{", "}" },
      { "%(", ")" }, -- % to escape lua pattern char
      { "%[", "]" }, -- % to escape lua pattern char
   },
}

require("which-key").setup {}
require("rainbow_csv").setup()

-- undotree
vim.g.undotree_WindowLayout = 2
vim.g.undotree_ShortIndicators = 1
vim.g.undotree_SplitWidth = 30
vim.g.undotree_DiffpanelHeight = 10
vim.g.undotree_SetFocusWhenToggle = 1
vim.g.undotree_DiffCommand = "diff -u"
