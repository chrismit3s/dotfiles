-- setup lsp-installer
require("nvim-lsp-installer").setup {
    ensure_installed = { "tsserver", "eslint", "texlab", "pyright", "jdtls", "clangd", "jsonls", "rust_analyzer", "clojure_lsp", "ltex" },
}

-- setup lsp-signature to show the current function signature
require("lsp_signature").setup {
  bind = true,
  handler_opts = {
    border = "rounded",
  },
}

-- UI tweaks
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
  vim.lsp.handlers.hover, {
    border = "rounded"
  }
)

vim.diagnostic.config {
    float = { border = "rounded" },
}

-- setup lsp config
local lspconfig = require("lspconfig")
local util = require("lspconfig.util")
local lspcmp = require("cmp_nvim_lsp")
local MODES = require("consts").MODES

-- use an on_attach function to only map the following keys after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
    local opts = { remap = false, buffer = bufnr }
    vim.keymap.set(MODES.normal, "<localleader>ld", vim.lsp.buf.definition, opts)
    vim.keymap.set(MODES.normal, "<localleader>lt", vim.lsp.buf.type_definition, opts)
    vim.keymap.set(MODES.normal, "<localleader>lr", vim.lsp.buf.references, opts)
    vim.keymap.set(MODES.normal, "<localleader>lh", vim.lsp.buf.hover, opts)
    vim.keymap.set(MODES.normal, "<localleader>lR", vim.lsp.buf.rename, opts)
    vim.keymap.set(MODES.normal, "<localleader>la", vim.lsp.buf.code_action, opts)
    vim.keymap.set(MODES.normal, "<localleader>ln", vim.diagnostic.goto_next, opts)
    vim.keymap.set(MODES.normal, "<localleader>lp", vim.diagnostic.goto_prev, opts)
    vim.keymap.set(MODES.normal, "<localleader>li", vim.diagnostic.open_float, opts)

    if client["name"] == "clangd" then
        vim.keymap.set(MODES.normal, "<localleader>lc", function() vim.cmd("ClangdSwitchSourceHeader") end, opts)
    end
end

local capabilities = lspcmp.default_capabilities()
local lsps = { "tsserver", "texlab", "pyright", "jdtls", "clangd", "jsonls", "hls", "clojure_lsp", "ltex" }
for _, lsp in ipairs(lsps) do
    lspconfig[lsp].setup {
        capabilities = capabilities,
        on_attach = on_attach,
        flags = {
            debounce_text_changes = 200,
        },
    }
end

lspconfig["eslint"].setup {
    capabilities = capabilities,
    on_attach = on_attach,
    root_dir = util.root_pattern(".git", "package.json"),
    flags = {
        debounce_text_changes = 200,
    },
}
lspconfig["rust_analyzer"].setup {
    capabilities = capabilities,
    on_attach = on_attach,
    flags = {
        debounce_text_changes = 200,
    },
    settings = {
        ["rust-analyzer"] = {
            checkOnSave = {
                allFeatures = true,
                overrideCommand = {
                    "cargo", "clippy", "--workspace", "--message-format=json",
                    "--all-targets", "--all-features"
                }
            }
        }
    }
}
