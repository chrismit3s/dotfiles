local M = {}

function M.bufs_by_type(loaded_only)
    loaded_only = (loaded_only == nil and true or loaded_only)
    by_type = { normal = {}, acwrite = {}, help = {}, nofile = {}, nowrite = {}, quickfix = {}, terminal = {}, prompt = {} }
    for _, bufname in pairs(vim.api.nvim_list_bufs()) do
       if (not loaded_only) or vim.api.nvim_buf_is_loaded(bufname) then
           buftype = vim.api.nvim_buf_get_option(bufname, 'buftype')
           buftype = buftype ~= '' and buftype or 'normal'
           table.insert(by_type[buftype], bufname)
       end
    end
    return by_type
end

function M.bufs_of_type(type, loaded_only)
    if type == nil then
        error("type is required for bufs_of_type")
    end
    loaded_only = (loaded_only == nil and true or loaded_only)
    type = (type == "normal" and "" or type)
    of_type = {}
    for _, bufname in pairs(vim.api.nvim_list_bufs()) do
       if (not loaded_only) or vim.api.nvim_buf_is_loaded(bufname) then
           if vim.api.nvim_buf_get_option(bufname, 'buftype') == type then
               table.insert(of_type, bufname)
           end
       end
    end
    return of_type
end

return M
