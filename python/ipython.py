c = get_config()

c.InteractiveShellApp.extensions = ["autoreload"]
c.InteractiveShellApp.exec_lines = [
    "%autoreload 2",
    "import numpy as np",
    "import scipy as sp",
    "import requests as rq",
]

c.InteractiveShell.editor = "nvim"
c.InteractiveShell.colors = "LightBG"
c.InteractiveShell.confirm_exit = False
c.InteractiveShell.deep_reload = True
