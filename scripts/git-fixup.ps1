# check number of arguments
if ($args.count -gt 1) {
    echo "usage: git fixup <commit-hash>"
    exit 1
}

# create fixup commit and rebase it
$hash=$args[0]
git commit --fixup=$hash
git -c sequence.editor="powershell -c ForEach-Object" rebase --interactive --autosquash --autostash "$hash~1"
