# parse args
switch ($args.count) {
    0 {
        $n=5
        $params=@()
    }
    1 {
        $n=$args[0]
        $params=@()
    }
    2 {
        $n=$args[1]
        $params=@($args[0])
    }
    Default {
        $n=$args[-1]
        $params=@($args[0..($args.count - 2)])
    }
}

# view last couple commits
git log --max-count=$n --decorate --pretty=oneline $params
